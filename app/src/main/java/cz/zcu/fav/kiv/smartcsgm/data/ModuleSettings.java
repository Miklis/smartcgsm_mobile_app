package cz.zcu.fav.kiv.smartcsgm.data;

import cz.zcu.fav.kiv.smartcsgm.bluetooth.ModuleType;

public class ModuleSettings {
   private final String moduleName;
   private final int moduleID;
   private final ModuleType moduleType;
   private final int readInterval;
   private final int minReadInt;
   private int maxReadInt;
   private boolean sleep;

    public ModuleSettings(String moduleName, int moduleID, ModuleType moduleType, int readInterval, int minReadInt, int maxReadInt, boolean sleep) {
        this.moduleName = moduleName;
        this.moduleID = moduleID;
        this.moduleType = moduleType;
        this.readInterval = readInterval;
        this.minReadInt = minReadInt;
        this.maxReadInt = maxReadInt;
        this.sleep = sleep;
    }

    public String getModuleName() {
        return moduleName;
    }

    public int getModuleID() {
        return moduleID;
    }

    public ModuleType getModuleType() {
        return moduleType;
    }

    public int getReadInterval() {
        return readInterval;
    }

    public int getMinReadInt() {
        return minReadInt;
    }

    public int getMaxReadInt() {
        return maxReadInt;
    }

    public boolean isSleep() {
        return sleep;
    }
}
