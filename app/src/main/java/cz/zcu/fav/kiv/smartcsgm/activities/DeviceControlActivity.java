package cz.zcu.fav.kiv.smartcsgm.activities;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;


import cz.zcu.fav.kiv.smartcsgm.R;
import cz.zcu.fav.kiv.smartcsgm.bluetooth.BluetoothLeService;
import cz.zcu.fav.kiv.smartcsgm.bluetooth.BLEProtocol;
import cz.zcu.fav.kiv.smartcsgm.dialogs.ConnectDialog;
import cz.zcu.fav.kiv.smartcsgm.tcp.TCPClient;

public class DeviceControlActivity extends AppCompatActivity {
    private final static String TAG = DeviceControlActivity.class.getSimpleName();

    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";

    private boolean dataFrag = true;
    private TextView mConnectionState;
    private EditText mDataField;
    private String mDeviceName;
    private String mDeviceAddress;
    private BluetoothLeService mBluetoothLeService;
    private boolean mConnected = false;
    private BLEProtocol BLEProtocol;
    private boolean closed = false;


    private byte[] receiveBuffer;

    public boolean ismConnected(){
        return mConnected;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);
        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);
        mConnectionState = findViewById(R.id.textView_state_val);
        this.closed = false;

        BLEProtocol = BLEProtocol.getInstance();
        BLEProtocol.init(this);

        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);
        }
       // TCPClient.initialize("192.168.1.48", 9999, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    /**
     * Method handle bluetooth service lifecycle
     */
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };


    /**
     *  Handles various events fired by the Service.
     *  ACTION_GATT_CONNECTED: connected to a GATT server.
     *  ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
     *  ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
     *  ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
     *  or notification operations.
     */
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                updateConnectionState(R.string.connected);
                invalidateOptionsMenu();
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                updateConnectionState(R.string.disconnected);
                cz.zcu.fav.kiv.smartcsgm.bluetooth.BLEProtocol.getInstance().setDisconnected();
                invalidateOptionsMenu();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                boolean en =false;
                if(mBluetoothLeService != null){
                    en = mBluetoothLeService.setCharacteristicNotification(true);
                }

                if(en == true){
                    Log.i("Notify", "Set enabled");
                }else{
                    Log.i("Notify", "Something went wrong");
                }
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                receiveBuffer = intent.getByteArrayExtra(BluetoothLeService.EXTRA_DATA);
                BLEProtocol.processRecievedData(receiveBuffer);
            }
        }
    };


    /**
     * Wrapper for sending data via BLE
     * @param arr data
     */
    public void writeCharacteristic(byte[] arr) {
        if(mBluetoothLeService != null){
            mBluetoothLeService.writeCharacteristic(arr);
        }

    }



    @Override
    protected void onPause() {
        super.onPause();
        //unregisterReceiver(mGattUpdateReceiver);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(closed == false){
            closeProcedure();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.control_menu, menu);

        if(mConnected == true){
            menu.findItem(R.id.menu_connect).setVisible(false);
        }else{
            menu.findItem(R.id.menu_connect).setVisible(true);
        }

        if (dataFrag == true) {
            menu.findItem(R.id.menu_data).setVisible(false);
            menu.findItem(R.id.menu_settings_info).setVisible(true);
        } else {
            menu.findItem(R.id.menu_data).setVisible(true);
            menu.findItem(R.id.menu_settings_info).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_control);
        switch (item.getItemId()) {
            case R.id.menu_home:
                final Intent intent = new Intent(this, DeviceScanActivity.class);
               //mBluetoothLeService.disconnect();
                closeProcedure();
                startActivity(intent);
                return true;
            case R.id.menu_settings_info:
                dataFrag = false;
                navController.navigate(R.id.action_data_to_settings);
                return true;
            case R.id.menu_connect:
                mBluetoothLeService.connect(mDeviceAddress);
                return true;
            case R.id.menu_data:
                dataFrag = true;
                navController.navigate(R.id.action_settings_to_data);
                return true;
            case R.id.menu_connect_to_server:
                ConnectDialog dialog = new ConnectDialog(this);
                dialog.show(getSupportFragmentManager(), "SmartCSGM connection");
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Updata connection state textView
     *
     * @param resourceId textView ID
     */
    private void updateConnectionState(final int resourceId) {
        runOnUiThread(() -> mConnectionState.setText(resourceId));
    }

    /**
     * Call all necessary methods for closing activity
     */
    private void closeProcedure(){
        byte[] arr = BLEProtocol.getInstance().buildStopMessage();
        TCPClient.getInstance().closeConnection();
        writeCharacteristic(arr);
        //mBluetoothLeService.disconnect();
        BLEProtocol.getInstance().stop();
        unregisterReceiver(mGattUpdateReceiver);
        unbindService(mServiceConnection);
        mBluetoothLeService = null;
        BLEProtocol.getInstance().setDisconnected();
        this.closed = true;
    }


    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }
}