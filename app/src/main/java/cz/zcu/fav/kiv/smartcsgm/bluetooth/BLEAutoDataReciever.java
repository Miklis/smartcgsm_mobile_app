package cz.zcu.fav.kiv.smartcsgm.bluetooth;

import android.util.Log;

import java.util.concurrent.locks.ReentrantLock;

import cz.zcu.fav.kiv.smartcsgm.activities.DeviceControlActivity;

public class BLEAutoDataReciever implements Runnable {

    private static final int TIMEOUT = 2000;
    ReentrantLock lock = new ReentrantLock();
    private DeviceControlActivity deviceControlActivity;
    private volatile boolean processing = true;
    private boolean autoSendingDataRequest = false;
    private boolean initMessageSend = false;
    private boolean startMessageRecievedSuccesfuly = false;

    public BLEAutoDataReciever(DeviceControlActivity deviceControlActivity) {
        this.deviceControlActivity = deviceControlActivity;
    }


    public void setAuto(boolean enabled){
        autoSendingDataRequest = enabled;
    }

    public void  stopThread(){
        getSetVar(false, true);
        processing = false;
    }

    public boolean isAutoSendingDataRequest() {
        return autoSendingDataRequest;
    }

    public void setStartMessageRecieved(boolean enabled){
        synchronized (lock){
            startMessageRecievedSuccesfuly = enabled;
            Log.i("Recieved", "Start Ack recieved");
        }
    }

    private synchronized boolean getSetVar(boolean processing, boolean set){
        if(set == true){
            this.processing = processing;
        }

        return processing;

    }

    @Override
    public void run() {
        while (getSetVar(processing, false) == true) {
            Log.i("AutoDat", "loop" + processing);
            try {
                Thread.sleep(TIMEOUT);
            } catch (InterruptedException e) {
           //     Log.e("AutoReciever", e.getMessage());
            }
            synchronized (lock){
                if(startMessageRecievedSuccesfuly == false && deviceControlActivity.ismConnected() == true){
                    deviceControlActivity.writeCharacteristic(BLEProtocol.getInstance().buildStartMessage());
                }
            }

            if (deviceControlActivity != null && autoSendingDataRequest != false) {
                deviceControlActivity.writeCharacteristic(BLEProtocol.getInstance().buildDataRequestAllMessage());
                Log.i("AutoDat", "send data req");
            }
        }
    }
}
