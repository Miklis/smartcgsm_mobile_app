package cz.zcu.fav.kiv.smartcsgm.data;

import android.widget.CheckBox;
import android.widget.TextView;

public class ModuleSettingUI {
    private TextView id;

    private TextView readInt;

    private CheckBox sleepBox;

    public ModuleSettingUI(TextView id, TextView readInt, CheckBox sleeBox) {
        this.id = id;
        this.readInt = readInt;
        this.sleepBox = sleeBox;
    }


    public int getId(){
        if(id != null){
            return Integer.parseInt(id.getText().toString());
        }else{
            return -1;
        }

    }

    public int getReadInt(){
        if(readInt != null){
            String aa = readInt.getText().toString();
            return Integer.parseInt(aa);
        }else{
            return -1;
        }

    }

    public boolean isSleep(){
        if(sleepBox != null){
            return sleepBox.isChecked();
        }else{
            return false;
        }

    }
}
