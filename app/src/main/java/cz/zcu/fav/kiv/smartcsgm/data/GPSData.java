package cz.zcu.fav.kiv.smartcsgm.data;

import java.util.Arrays;

public class GPSData {
    public static final int DATA_LEN = 34;

    private final int lat;
    private final int lon;
    private final String time;

    public static GPSData createData(byte[] arr){
        byte[] arrLat = Arrays.copyOfRange(arr, 3, 7);
        byte[] arrLon = Arrays.copyOfRange(arr, 12, 16);
        byte[] arrTim = Arrays.copyOfRange(arr, 21, 29);

        int lat = (int)java.nio.ByteBuffer.wrap(arrLat).order(java.nio.ByteOrder.LITTLE_ENDIAN).getShort();
        int lon = (int)java.nio.ByteBuffer.wrap(arrLon).order(java.nio.ByteOrder.LITTLE_ENDIAN).getShort();
        String time = new String(arrTim);

        return new GPSData(lat, lon, time);

    }

    public GPSData(int lat, int lon, String time) {
        this.lat = lat;
        this.lon = lon;
        this.time = time;
    }

    public int getLat() {
        return lat;
    }

    public int getLon() {
        return lon;
    }

    public String getTime() {
        return time;
    }
}
