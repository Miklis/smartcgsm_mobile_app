package cz.zcu.fav.kiv.smartcsgm.fragments;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import cz.zcu.fav.kiv.smartcsgm.R;
import cz.zcu.fav.kiv.smartcsgm.activities.DeviceControlActivity;
import cz.zcu.fav.kiv.smartcsgm.bluetooth.BLEProtocol;
import cz.zcu.fav.kiv.smartcsgm.data.Modules;
import cz.zcu.fav.kiv.smartcsgm.tcp.TCPClient;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class DataFragment extends Fragment {

    Button buttonData;
    Button buttonautoData;
    Button buttonSendDataToServer;

    private DeviceControlActivity deviceControlActivity;
    private boolean startProccesing = false;
    private boolean autoDataRecieving = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Activity activity = (Activity) getContext();
        if(getContext() instanceof  DeviceControlActivity){
            deviceControlActivity = (DeviceControlActivity)activity;
        }
        BLEProtocol.getInstance().setDataFragment(this);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_data, container, false);

        assingUIElements(view);
        setButtonsEnabled(false);
        if(BLEProtocol.getInstance().isstartAckRecived()){
            setButtonsEnabled(true);
        }else{
            setButtonsEnabled(false);
        }

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.menu_data).setVisible(false);
        menu.findItem(R.id.menu_settings_info).setVisible(true);
    }

    public void setButtonsEnabled(boolean enabled){
        setButtonautoDataEnabled(enabled);
        setButtonDataEnable(enabled);
        setButtonSendDataToserverEnabled(enabled);
    }

    public void setButtonSendDataToserverEnabled(boolean enabled){
        buttonSendDataToServer.setEnabled(enabled);
    }

    public void setButtonDataEnable(boolean enable){
        buttonData.setEnabled(enable);
    }

    public void setButtonautoDataEnabled(boolean enabled){
        buttonautoData.setEnabled(enabled);
    }


    /***
     * Display gather data from protocol to 2D view.
     *
     * @param data data from modules
     */
    public void setValuesToUiElements(Modules data){

        if(getView() != null) {
            TextView taX = getView().findViewById(R.id.textView_accelerometer_x_val);
            TextView taY = getView().findViewById(R.id.textView_accelerometer_y_val);
            TextView taZ = getView().findViewById(R.id.textView_accelerometer_z_val);

            taX.setText(data.getAccData().getX() + "");
            taY.setText(data.getAccData().getY() + "");
            taZ.setText(data.getAccData().getZ() + "");

            TextView tgX = getView().findViewById(R.id.textView_gyroscope_x_val);
            TextView tgY = getView().findViewById(R.id.textView_gyroscope_y_val);
            TextView tgZ = getView().findViewById(R.id.textView_gyroscope_z_val);

            tgX.setText(data.getGyroData().getX() + "");
            tgY.setText(data.getGyroData().getY() + "");
            tgZ.setText(data.getGyroData().getZ() + "");

            TextView tLat = getView().findViewById(R.id.textView_gps_lat_val);
            TextView tLon = getView().findViewById(R.id.textView_gps_lon_val);
            TextView tTim = getView().findViewById(R.id.textView_time_val);

            tLat.setText(data.getGpsData().getLat() + "");
            tLon.setText(data.getGpsData().getLon() + "");
            tTim.setText(data.getGpsData().getTime());

            TextView temp1 = getView().findViewById(R.id.textView_temp_cels1_val);
            temp1.setText(data.getTemp1Data().getCels() + "");

            TextView temp2 = getView().findViewById(R.id.textView_temp_cels2_val);
            temp2.setText(data.getTemp2Data().getCels() + "");

            TextView mov0 = getView().findViewById(R.id.textView_mov_in1_val);
            TextView mov1 = getView().findViewById(R.id.textView_mov_in2_val);
            TextView mov2 = getView().findViewById(R.id.textView_mov_in3_val);
            TextView mov3 = getView().findViewById(R.id.textView_mov_in4_val);
            TextView mov4 = getView().findViewById(R.id.textView_mov_in5_val);

            int tIn0 = data.getMoveData().getMovIn0() + Integer.parseInt(mov0.getText().toString());
            int tIn1 = data.getMoveData().getMovIn1() + Integer.parseInt(mov1.getText().toString());
            int tIn2 = data.getMoveData().getMovIn2() + Integer.parseInt(mov2.getText().toString());
            int tIn3 = data.getMoveData().getMovIn3() + Integer.parseInt(mov3.getText().toString());
            int tIn4 = data.getMoveData().getMovIn4() + Integer.parseInt(mov4.getText().toString());

            data.getMoveData().setMovIn0(tIn0);
            data.getMoveData().setMovIn1(tIn1);
            data.getMoveData().setMovIn2(tIn2);
            data.getMoveData().setMovIn3(tIn3);
            data.getMoveData().setMovIn4(tIn4);

            mov0.setText(data.getMoveData().getMovIn0() + "");
            mov1.setText(data.getMoveData().getMovIn1() + "");
            mov2.setText(data.getMoveData().getMovIn2() + "");
            mov3.setText(data.getMoveData().getMovIn3() + "");
            mov4.setText(data.getMoveData().getMovIn4() + "");

            if(autoDataRecieving == true){
               deviceControlActivity.writeCharacteristic(BLEProtocol.getInstance().buildDataRequestAllMessage());
            }

        }else{
            Log.w("DataFragment data", "getview null");
        }

    }

    /**
     * Assing view to variables and add fuctions to buttons
     *
     * @param view
     */
    private void assingUIElements(View view){
      buttonData = view.findViewById(R.id.button_data_req);
      buttonautoData = view.findViewById(R.id.button_auto);
      buttonSendDataToServer = view.findViewById(R.id.button_send_data_to_server);

        setReqDataButton(buttonData);
        setAutoRecievingButton(buttonautoData);
        setButtonSendDataToServer(buttonSendDataToServer);
    }


    private void setButtonSendDataToServer(Button button){
        button.setOnClickListener(view -> {
            if(TCPClient.getInstance().isConnected() == true){
                TCPClient.getInstance().sendMulitpleData();
            }else{
                Toast.makeText(getContext(), "Cant send data. Client is not connected to server", Toast.LENGTH_LONG).show();
            }

        });
    }

    private void setReqDataButton(Button button){
        button.setOnClickListener(v -> {
            byte[] arr = BLEProtocol.getInstance().buildDataRequestAllMessage();
            deviceControlActivity.writeCharacteristic(arr);
        });
    }

    private void setAutoRecievingButton(Button button){

        if(BLEProtocol.getInstance().isAutoSending() == true){
            button.setText("stop");
        }else {
            button.setText("start");
        }


        button.setOnClickListener(view -> {
            if(deviceControlActivity != null){
                if(this.autoDataRecieving == false){
                    button.setText("stop");
                    BLEProtocol.getInstance().setAutoData(true);
                    this.autoDataRecieving = true;
                }else{
                    button.setText("start");
                    BLEProtocol.getInstance().setAutoData(false);
                    this.autoDataRecieving = false;
                }
            }
        });
    }
}