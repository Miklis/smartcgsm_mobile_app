package cz.zcu.fav.kiv.smartcsgm.data;

public class ModulesBufferData {

    private Modules modules;
    private int timestamp;

    public ModulesBufferData(Modules modules, int timestamp) {
        this.timestamp = timestamp;
        this.modules = modules;
    }

    public Modules getModules() {
        return modules;
    }

    public int getTimestamp() {
        return timestamp;
    }
}
