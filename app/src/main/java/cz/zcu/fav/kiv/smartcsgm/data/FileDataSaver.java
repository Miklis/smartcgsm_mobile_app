package cz.zcu.fav.kiv.smartcsgm.data;

import android.os.Build;
import android.os.Environment;
import android.util.Log;

import androidx.annotation.RequiresApi;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import cz.zcu.fav.kiv.smartcsgm.activities.DeviceControlActivity;

public class FileDataSaver {
    public static final String ACCEL_FILE = "accel.txt";
    public static final String GYRO_FILE = "gyro.txt";
    public static final String TEMP1_FILE = "temp1.txt";
    public static final String TEMP2_FILE = "temp2.txt";
    public static final String GPS_FILE = "gps.txt";
    public static final String MOVE_FILE = "move.txt";

    DeviceControlActivity deviceControlActivity;

    public FileDataSaver(DeviceControlActivity deviceControlActivity) {
        this.deviceControlActivity = deviceControlActivity;
    }

    public void saveAccel(AccelerometrData data) {

        String s = String.format("%s;x;%d;y;%d;z;%d", getStringTime(), data.getX(), data.getY(), data.getZ());
        try {
            File f = new File(deviceControlActivity.getExternalFilesDir(null), ACCEL_FILE);
            FileWriter fw = new FileWriter(f, true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(s);
            bw.newLine();
            bw.close();
        } catch (Exception e) {
            Log.e("saveAccel", e.getMessage());
        }
    }

    public void saveGyro(GyroscopeData data) {
        String s = String.format("%s;x;%d;y;%d;z;%d", getStringTime(), data.getX(), data.getY(), data.getZ());
        try {
            File f = new File(deviceControlActivity.getExternalFilesDir(null), GYRO_FILE);
            FileWriter fw = new FileWriter(f, true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(s);
            bw.newLine();
            bw.close();
        } catch (Exception e) {
            Log.e("saveGyro", e.getMessage());
        }
    }

    public void saveTemp(TemperatureData data, String fileName) {
        String s = String.format("%s,cels;%d", getStringTime(), data.getCels());
        try {
            File f = new File(deviceControlActivity.getExternalFilesDir(null), fileName);
            FileWriter fw = new FileWriter(f, true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(s);
            bw.newLine();
            bw.close();
        } catch (Exception e) {
            Log.e("saveTemp", e.getMessage());
        }
    }

    public void saveGPS(GPSData data) {
        String s = String.format("%s;lat;%d;lon;%d;time;%s", getStringTime(), data.getLat(), data.getLon(), data.getTime());
        try {
            File f = new File(deviceControlActivity.getExternalFilesDir(null), GPS_FILE);
            FileWriter fw = new FileWriter(f, true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(s);
            bw.newLine();
            bw.close();
        } catch (Exception e) {
            Log.e("saveGPS", e.getMessage());
        }
    }

    public void saveMove(MovementData data) {
        String s = String.format("%s,in0;%d;in1;%d;in2;%d;in3;%d;in4;%d", getStringTime() ,data.getMovIn0(), data.getMovIn1(), data.getMovIn2(), data.getMovIn3(), data.getMovIn4());
        try {
            File f = new File(deviceControlActivity.getExternalFilesDir(null), MOVE_FILE);

            Log.i("file len",f.length() + "");
            FileWriter fw = new FileWriter(f, true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(s);
            bw.newLine();
            bw.close();
        } catch (Exception e) {
            Log.e("saveGPS", e.getMessage());
        }
    }


    private String getStringTime() {
        DateTimeFormatter dtf = null;
        LocalDateTime now = null;
        String time = "";
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            now = LocalDateTime.now();
            dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            time = dtf.format(now);
        }

        return time;
    }

}
