package cz.zcu.fav.kiv.smartcsgm.bluetooth;

public enum MessageType {
    START_MESSAGE,
    INITIAL_MESSAGE,
    SETTINGS_MESSAGE,
    DATA_MESSAGE,
    REQUEST_MESSAGE,
    STOP_MESSAGE,
    ACK_MESSAGE,
    NACK_MESSAGE;

    public static MessageType getMessageType(int numType){
        switch (numType){
            case 1:
                return START_MESSAGE;
            case 2:
                return INITIAL_MESSAGE;
            case 3:
                return SETTINGS_MESSAGE;
            case 4:
                return DATA_MESSAGE;
            case 5:
                return REQUEST_MESSAGE;
            case 6:
                return STOP_MESSAGE;
            case 7:
                return ACK_MESSAGE;
            case 8:
                return NACK_MESSAGE;
            default:
                return null;
        }

    }
}
