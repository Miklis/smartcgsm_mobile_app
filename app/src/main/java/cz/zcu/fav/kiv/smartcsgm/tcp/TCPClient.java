package cz.zcu.fav.kiv.smartcsgm.tcp;

import android.util.Log;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Struct;
import java.util.Arrays;

import cz.zcu.fav.kiv.smartcsgm.activities.DeviceControlActivity;
import cz.zcu.fav.kiv.smartcsgm.bluetooth.BLEProtocol;
import cz.zcu.fav.kiv.smartcsgm.data.ModulesBufferData;

public class TCPClient extends Thread {
    private static TCPClient INSTANCE;



    private final Object lock = new Object();

    private final Object socketLock = new Object();

    private InputStreamReader isr;

    private BufferedReader br;

    private OutputStreamWriter osw;

    private BufferedOutputStream baf;

    private DeviceControlActivity activity;

    private Socket socket;

    private boolean socketStart = false;

    private String ip;

    private int port;

    private TCPClient(){
        this.connect();
    }

    public static void initialize(String ip, int port, DeviceControlActivity deviceControlActivity){



        if(INSTANCE != null){
            INSTANCE = null;
            INSTANCE = new TCPClient();
            INSTANCE.setIp(ip);
            INSTANCE.setPort(port);

        }else{
            INSTANCE = new TCPClient();
            INSTANCE.activity = deviceControlActivity;
            INSTANCE.setIp(ip);
            INSTANCE.setPort(port);
        }
    }

    public static TCPClient getInstance(){
        if(INSTANCE == null){
            INSTANCE = new TCPClient();
        }

        return INSTANCE;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isConnected(){

        if(socket != null){
            return socket.isConnected();
        }

        return false;
    }

    @Override
    public void run() {
        boolean failed = false;

        try {
            this.socket = new Socket(ip, port);
            this.isr = new InputStreamReader(socket.getInputStream());
            this.br = new BufferedReader(isr);
            this.baf = new BufferedOutputStream(socket.getOutputStream());
            setSocketStart(true);
        }catch (ConnectException e){
            Log.i("Sockete_error", "Socket problem" ,e);
            failed = true;
        } catch (UnknownHostException e) {
            Log.i("Sockete_error", "Socket problem" ,e);
            failed = true;
        } catch (IOException e) {
            Log.i("Sockete_error", "Socket problem" ,e);
            failed = true;
        }catch (Exception e) {
            Log.i("Sockete_error", "Socket problem", e);
            failed = true;
        }

        if(failed == true){
            activity.runOnUiThread(() -> Toast.makeText(activity, "Cant connect", Toast.LENGTH_SHORT));
            setSocketStart(false);
            INSTANCE = null;
            try {
                if(socket != null){
                    socket.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            activity.runOnUiThread(() -> Toast.makeText(activity, "Cant connected", Toast.LENGTH_SHORT));
            Log.i("Socket", "Socket created");
        }
    }

    /**
     * Send data to server
     *
     * @param mess data
     */
    public synchronized void sendMessage(byte[] mess){

        Log.d("SENDING", mess.toString());

        Thread thread = new Thread("New Thread") {
            public void run(){
                while(getSocketStarStat() == false){
                    //waitin until socket thread start
                }
                if(socket == null){
                    return;
                }


                try {
                    getBufferedStream().write(mess);
                    getBufferedStream().flush();
                } catch (IOException e) {
                    Log.e("Write", e.getMessage());
                }

                Log.i("MessageSock", "Message send: " + mess.toString());
            }
        };
        thread.start();
    }


    /***
     * Send all data from buffer to server
     *
     * and clear buffer
     */
    public void sendMulitpleData(){
        ModulesBufferData[] bufferData = BLEProtocol.getInstance().getBufferData();

        for(int i = 0; i < bufferData.length; i++){

            if(bufferData[i] != null){
                sendMessage(TCPProtocol.getInstance().createMessage(bufferData[i]));
            }
        }

        Arrays.fill(bufferData, null);
    }

    private void connect(){

        this.start();
    }

    private void setSocketStart(boolean state){
        synchronized (lock){
            socketStart = state;
        }
    }

    private BufferedOutputStream getBufferedStream(){
        synchronized (socketLock){
            return baf;
        }
    }

    /**
     * Gets socket start state
     *
     * @return socket state
     */
    private  boolean getSocketStarStat(){
        synchronized (lock){
            return socketStart;
        }
    }


    /**
     * Close server comunication
     */
    public void closeConnection(){
        if(socket == null){
            return;
        }
        try {
            socket.close();
            if(br != null){
                br.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.stop();
    }

}
