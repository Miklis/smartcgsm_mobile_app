package cz.zcu.fav.kiv.smartcsgm.bluetooth;

import android.util.Log;
import android.widget.Toast;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;

import cz.zcu.fav.kiv.smartcsgm.activities.DeviceControlActivity;
import cz.zcu.fav.kiv.smartcsgm.data.AccelerometrData;
import cz.zcu.fav.kiv.smartcsgm.data.FileDataSaver;
import cz.zcu.fav.kiv.smartcsgm.data.GPSData;
import cz.zcu.fav.kiv.smartcsgm.data.GyroscopeData;
import cz.zcu.fav.kiv.smartcsgm.data.ModuleSettingUI;
import cz.zcu.fav.kiv.smartcsgm.data.ModuleSettings;
import cz.zcu.fav.kiv.smartcsgm.data.Modules;
import cz.zcu.fav.kiv.smartcsgm.data.ModulesBufferData;
import cz.zcu.fav.kiv.smartcsgm.data.MovementData;
import cz.zcu.fav.kiv.smartcsgm.data.TemperatureData;
import cz.zcu.fav.kiv.smartcsgm.fragments.DataFragment;
import cz.zcu.fav.kiv.smartcsgm.fragments.SettingsFragment;
import cz.zcu.fav.kiv.smartcsgm.tcp.TCPClient;

public class BLEProtocol {


    private static BLEProtocol instance;


    private static final int DATA_BUFFER_LEN = 100;

    /* Header*/
    private static final int OPCODE_LEN = 1;
    private static final int HEADER_LEN = 7;
    private static final int MESSAGE_LEN_LEN = 2;
    private static final int MAX_MESSAGE_LEN = 2048;
    private static final int MAX_PAKET_LEN = 20;

    /* Message end */
    private static final int MESSAGE_END_LEN = 2;
    private static final byte CR = 13;
    private static final byte LF = 10;

    /* Connected device modules */
    private static final int SENSORS_NUM = 6;

    /* Message types */
    private static final byte OPCODE_START     = 1;
    private static final byte OPCODE_INFO      = 2;
    private static final byte OPCODE_SETTINGS  = 3;
    private static final byte OPCODE_DATA      = 4;
    private static final byte OPCODE_REQUEST   = 5;
    private static final byte OPCODE_STOP      = 6;
    private static final byte OPCODE_ACK       = 7;
    private static final byte OPCODE_NACK      = 8;

    private static final int MESSAGE_SUB_TYPE_LEN = 1;

    /* Message subtypes */
    private static final byte SET_TYPE_MODULES   = 1;
    private static final byte SET_TYPE_MAIN_LOOP = 2;

    /* Main loop settings message*/
    private static final int LOOP_SET_MESSAGE_LEN = 3;

    /* Module settings message */
    private static final int ONE_MODULE_SETTINGS_LEN = 4;


    private final long startTime;

    private DataFragment dataFragment;
    private SettingsFragment settingsFragment;
    private DeviceControlActivity deviceControlActivity;

    private Thread autoDataReq;
    private BLEAutoDataReciever autoData;

    private FileDataSaver dataSaver;

    private ModulesBufferData[] bufferData = new ModulesBufferData[DATA_BUFFER_LEN];
    private int bufferDataIndex = 0;


    private boolean headerProccessed = false;
    private MessageType messageType;
    private int messageLen;
    private int processedLen = 0;
    private int messageTimeStamp;
    private boolean autoMessageRecieving = false;
    private byte[] messageBuffer = new byte[4096];

    private boolean startAckRecived = false;

    private BLEProtocol(){
        startTime = System.currentTimeMillis();
    }


    public static BLEProtocol getInstance(){
        if(instance == null){
            instance = new BLEProtocol();
        }
        return instance;
    }

    public boolean isstartAckRecived(){
        return startAckRecived;
    }

    public void init(DeviceControlActivity deviceControlActivity){
        this.deviceControlActivity = deviceControlActivity;
        autoData = new BLEAutoDataReciever(deviceControlActivity);
        autoDataReq = new Thread(autoData);
        dataSaver = new FileDataSaver(deviceControlActivity);
        autoDataReq.start();
        resetParametrs();
    }

    public void stop(){

        autoData.stopThread();
        autoDataReq.interrupt();
        dataSaver = null;
        resetParametrs();
    }

    public boolean isAutoSending(){
        return autoData.isAutoSendingDataRequest();
    }

    public void setAutoData(boolean enabled){
        if(autoData != null){
            autoData.setAuto(enabled);
        }
    }

    public void setDataFragment(DataFragment dataFragment){
        this.dataFragment = dataFragment;
    }

    public void setSettingsFragment(SettingsFragment settingsFragment){
        this.settingsFragment = settingsFragment;
    }

    /**
     * disable button for data requesting and buttont for auto requesting data
     */
    public void setDisconnected(){
        if(dataFragment != null){
            startAckRecived = false;

            if(dataFragment.isVisible()){
                dataFragment.setButtonsEnabled(false);
                autoData.setStartMessageRecieved(false);
            }


        }

        if(settingsFragment != null){
            if(settingsFragment.isVisible()){

            }
        }
    }

    public ModulesBufferData[] getBufferData() {
        return bufferData;
    }

    public byte[] buildStartMessage(){
        short mesLen = 0;
        byte[] arr = new byte[9];
        setHeader(arr, OPCODE_START, mesLen);

        arr[7] = CR;
        arr[8] = LF;

        return arr;
    }

    public byte[] buildStopMessage(){
        short mesLen = 0;
        byte[] arr = new byte[9];
        setHeader(arr, OPCODE_STOP, mesLen);

        arr[7] = CR;
        arr[8] = LF;

        return arr;
    }

    public byte[] buildInfoReqMessage(){
        short mesLen = 1;
        byte[] arr = new byte[10];
        setHeader(arr, OPCODE_REQUEST, mesLen);

        arr[7] = 2;
        arr[8] = CR;
        arr[9] = LF;

        return arr;
    }

    public byte[] buildDataRequestAllMessage(){
        short mesLen = SENSORS_NUM + MESSAGE_SUB_TYPE_LEN;
        byte[] arr = new byte[HEADER_LEN + mesLen + MESSAGE_END_LEN];
        setHeader(arr, OPCODE_REQUEST, mesLen);

        arr[HEADER_LEN] = 1; //reguest data
        arr[HEADER_LEN + 1] = 1; //accel
        arr[HEADER_LEN + 2] = 2; //gyro
        arr[HEADER_LEN + 3] = 3; //temp1;
        arr[HEADER_LEN + 4] = 4; //temp2
        arr[HEADER_LEN + 5] = 5; //gps
        arr[HEADER_LEN + 6] = 6; //mov sens
        arr[HEADER_LEN + 7] = CR;
        arr[HEADER_LEN + 8] = LF;

        return arr;
    }

    public byte[][] buildModulesSettingMessage(ArrayList<ModuleSettingUI> moduleSettings){
        short mesLen = (short) (MESSAGE_SUB_TYPE_LEN + (moduleSettings.size() * ONE_MODULE_SETTINGS_LEN));
        byte[] arr = new byte[HEADER_LEN + mesLen + MESSAGE_END_LEN];
        setHeader(arr, OPCODE_SETTINGS, mesLen);

        arr[HEADER_LEN] = SET_TYPE_MODULES;

        for(int i = 0; i < moduleSettings.size(); i++){
            byte id = (byte) moduleSettings.get(i).getId();
            short read = (short) moduleSettings.get(i).getReadInt();
            byte sleep = 0;

            if (moduleSettings.get(i).isSleep() == true){
                sleep = 1;
            }else{
                sleep = 0;
            }



            ByteBuffer bbRead = ByteBuffer.allocate(2);
            bbRead.putShort(read);
            byte[] arrIntRead = bbRead.array();


            arr[HEADER_LEN + 1 + (ONE_MODULE_SETTINGS_LEN * i)] = id;
            arr[HEADER_LEN + 2 + (ONE_MODULE_SETTINGS_LEN * i)] = arrIntRead[1];
            arr[HEADER_LEN + 3 + (ONE_MODULE_SETTINGS_LEN * i)] = arrIntRead[0];
            arr[HEADER_LEN + 4 + (ONE_MODULE_SETTINGS_LEN * i)] =sleep;
        }

        arr[arr.length - 2] = CR;
        arr[arr.length - 1] = LF;

        return getMessageFragment(arr);
    }

    public byte[] buildMainLoopSettingsMessage(int interval){
        short mesLen = LOOP_SET_MESSAGE_LEN;
        byte[] arr = new byte[HEADER_LEN + mesLen + MESSAGE_END_LEN];
        setHeader(arr, OPCODE_SETTINGS, mesLen);

        short timeInt = (short) interval;

        ByteBuffer bb = ByteBuffer.allocate(2);
        bb.putShort(timeInt);
        byte[] arrInt = bb.array();

        arr[HEADER_LEN] = SET_TYPE_MAIN_LOOP;
        arr[HEADER_LEN + 1] = arrInt[1];
        arr[HEADER_LEN + 2] = arrInt[0];
        arr[HEADER_LEN + 3] = CR;
        arr[HEADER_LEN + 4] = LF;

        return arr;
    }

    public void processRecievedData( byte[] bytes){
       // byte[] bytes = message.getBytes();



        for(int i = 0; i < bytes.length; i++){
            if(processedLen >= MAX_MESSAGE_LEN){
                processedLen = 0;
            }
            this.messageBuffer[this.processedLen++] = bytes[i];
        }

        if(processedLen >= HEADER_LEN && headerProccessed == false){
           messageType = MessageType.getMessageType((int) messageBuffer[0]);
           messageLen =  java.nio.ByteBuffer.wrap(Arrays.copyOfRange(messageBuffer, 1, 3)).order(java.nio.ByteOrder.LITTLE_ENDIAN).getShort();
           messageTimeStamp =  java.nio.ByteBuffer.wrap(Arrays.copyOfRange(messageBuffer, 3, 7)).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt();
           headerProccessed = true;




           if(messageType == null){
               resetParametrs();
           }else{
               Log.i("header", messageType.name() + " " + messageLen + " " + messageTimeStamp);
           }
        }

        if(headerProccessed == true && messageLen == processedLen){
            proccessMessage();
        }

        int len = messageLen + HEADER_LEN + MESSAGE_END_LEN;

        if(processedLen == len){
            if(messageBuffer[processedLen - 1] == LF && messageBuffer[processedLen - 2] == CR){
                proccessMessage();
            }

            resetParametrs();
        }
    }

    private void proccessMessage(){
        switch (messageType){
            case DATA_MESSAGE:
                processDataMessage();
                break;
            case ACK_MESSAGE:
                if(startAckRecived == false){
                    autoData.setStartMessageRecieved(true);
                    startAckRecived = true;
                    dataFragment.setButtonsEnabled(true);
                }

                break;
            case INITIAL_MESSAGE:
                processInfoMessage();
                break;
            case NACK_MESSAGE:
        }
    }


    /***
     * Gether data form data message and call method which refresh data view.
     *
     * If auto sending is on call interupt on thread for autosending data request
     */
    private void processDataMessage(){
        AccelerometrData accelerometrData;
        GyroscopeData gyroscopeData;
        TemperatureData temperatureData1;
        TemperatureData temperatureData2;
        GPSData gpsData;
        MovementData movementData;


        int processedLen = HEADER_LEN;

        byte[] accelDataByte = Arrays.copyOfRange(messageBuffer, processedLen, processedLen + AccelerometrData.DATA_LEN);
        processedLen += AccelerometrData.DATA_LEN;
        accelerometrData = AccelerometrData.createData(accelDataByte);
        dataSaver.saveAccel(accelerometrData);

        byte[] gyroDataByte = Arrays.copyOfRange(messageBuffer, processedLen, processedLen + GyroscopeData.DATA_LEN);
        processedLen += GyroscopeData.DATA_LEN;
        gyroscopeData = GyroscopeData.createData(gyroDataByte);
        dataSaver.saveGyro(gyroscopeData);

        byte[] temperatureDataByte1 = Arrays.copyOfRange(messageBuffer, processedLen, processedLen + TemperatureData.DATA_LEN);
        processedLen += TemperatureData.DATA_LEN;
        temperatureData1 = TemperatureData.createData(temperatureDataByte1);
        dataSaver.saveTemp(temperatureData1, FileDataSaver.TEMP1_FILE);

        byte[] temperatureDataByte2 = Arrays.copyOfRange(messageBuffer, processedLen, processedLen + TemperatureData.DATA_LEN);
        processedLen += TemperatureData.DATA_LEN;
        temperatureData2 = TemperatureData.createData(temperatureDataByte2);
        dataSaver.saveTemp(temperatureData2, FileDataSaver.TEMP2_FILE);

        byte[] gpsDataByte = Arrays.copyOfRange(messageBuffer, processedLen, processedLen + GPSData.DATA_LEN);
        processedLen += GPSData.DATA_LEN;
        gpsData = GPSData.createData(gpsDataByte);
        dataSaver.saveGPS(gpsData);


        byte[] movementDataByte = Arrays.copyOfRange(messageBuffer, processedLen, processedLen + MovementData.DATA_LEN);
        processedLen += MovementData.DATA_LEN;
        movementData = MovementData.createData(movementDataByte);
        dataSaver.saveMove(movementData);

        Modules modules = new Modules(accelerometrData, gyroscopeData, temperatureData1, temperatureData2, gpsData, movementData);



       // TCPClient.getInstance().sendMessage(TCPProtocol.getInstance().createMessage(modules));
        saveDataToBuffer(modules);

        if(dataFragment != null && modules != null){
            dataFragment.setValuesToUiElements(modules);
        }

        if(autoData.isAutoSendingDataRequest() == true){
            if(autoDataReq.isAlive() == true){
                autoDataReq.interrupt();
            }
        }

    }

    /**
     * Gether module information from massege and call method which refresh module info view
     */
    private void processInfoMessage(){
        int processedBytes = 0;

        ArrayList<ModuleSettings> moduleSettings = new ArrayList<>();

        while(processedBytes <  messageLen){
            byte[] temp = new byte[4];

            temp[0] = messageBuffer[processedBytes + HEADER_LEN];
            int id = java.nio.ByteBuffer.wrap(temp).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt();;
            processedBytes++;

            temp[0] = messageBuffer[processedBytes + HEADER_LEN];
            int moduleTypeI = java.nio.ByteBuffer.wrap(temp).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt();
            ModuleType moduleType = ModuleType.getModuleType(moduleTypeI);
            processedBytes++;

            temp[0] = messageBuffer[processedBytes + HEADER_LEN];
            temp[1] = messageBuffer[(processedBytes + 1) + HEADER_LEN];
            int defaultReadInterval =  java.nio.ByteBuffer.wrap(temp).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt();
            processedBytes += 2;


            temp[0] = messageBuffer[processedBytes + HEADER_LEN];
            temp[1] = messageBuffer[(processedBytes + 1) + HEADER_LEN];
            int minReadInterval =  java.nio.ByteBuffer.wrap(temp).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt();
            processedBytes += 2;

            temp[0] = messageBuffer[processedBytes + HEADER_LEN];
            temp[1] = messageBuffer[(processedBytes + 1) + HEADER_LEN];
            int maxReadInterval = java.nio.ByteBuffer.wrap(temp).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt();
            processedBytes += 2;


            /**************** Sleep **************/

            temp[0] =  messageBuffer[processedBytes + HEADER_LEN];
            temp[1] = 0;
            boolean sleep;

            if(temp[0] == 0){
                sleep = false;
            }else{
                sleep = true;
            }
            processedBytes++;


            /**************** Desc len *************/

            temp[0] = messageBuffer[processedBytes + HEADER_LEN];

            int descLen =  java.nio.ByteBuffer.wrap(temp).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt();
            processedBytes++;


            String desc = new String(Arrays.copyOfRange(messageBuffer, processedBytes + HEADER_LEN, processedBytes + HEADER_LEN + descLen));
            processedBytes += descLen;

            ModuleSettings module = new ModuleSettings(desc, id, moduleType, defaultReadInterval, minReadInterval, maxReadInterval, sleep);
            moduleSettings.add(module);

        }

        if(settingsFragment != null && settingsFragment.isVisible() == true){
            settingsFragment.addDeviceInfo(moduleSettings);
        }
    }

    /***
     * Save module data to buffer
     *
     * @param module module data
     */
    private void saveDataToBuffer(Modules module){
        int unixTimeStamp = (int) (System.currentTimeMillis() / 1000L);

        bufferData[bufferDataIndex] = new ModulesBufferData(module, unixTimeStamp);
        bufferDataIndex++;
        bufferDataIndex = bufferDataIndex % DATA_BUFFER_LEN;
        if(TCPClient.getInstance().isConnected() == true){
            checkBufferFull();
        }else{
            Toast.makeText(deviceControlActivity, "Cant send data. Client is not connected", Toast.LENGTH_LONG);
        }

    }

    /**
     * Check if message buffer is full. In this case send all buffer data to server.
     */
    private void checkBufferFull(){
        int sum = 0;
        for(int i = 0; i < bufferData.length; i++){

            if (bufferData[i] != null) {
                sum++;
            }

            if(sum >= DATA_BUFFER_LEN) {
                Log.i("BLEProtocol", "Sending buffer");
                 TCPClient.getInstance().sendMulitpleData();
            }


        }
    }


    /**
     * Reset control parametr for recieving
     */
    private void resetParametrs(){
        this.messageBuffer = new byte[2048];
        this.messageLen = 0;
        this.processedLen = 0;
        this.messageTimeStamp = 0;
        this.headerProccessed = false;
    }

    /**
     * Prepare message header
     *
     * @param arr data
     * @param opcode message type
     * @param len message len
     */
    private void setHeader(byte[] arr, byte opcode, short len){
        arr[0] = opcode;
        setLen(arr, len);
        seTimestamp(arr);
    }

    private void seTimestamp(byte[] arr){
        int time = (int)(System.currentTimeMillis() - startTime);

        ByteBuffer bb = ByteBuffer.allocate(4);
        bb.putInt(time);

        byte[] timeB = bb.array();

        arr[3] = timeB[3];
        arr[4] = timeB[2];
        arr[5] = timeB[1];
        arr[6] = timeB[0];
    }


    /**
     * Set len of message budy in protokol header
     *
     * @param arr message
     * @param len body len
     */
    private void setLen(byte[] arr, short len){
        ByteBuffer bb = ByteBuffer.allocate(2);
        bb.putShort(len);
        byte[] lenB = bb.array();

        arr[1] = lenB[1];
        arr[2] = lenB[0];
    }

    /**
     * Build message fraqments, because is possible send only 20B of data in one fraqment over
     * BLE
     *
     * @param arr message
     * @return fraqmented message
     */
    private byte[][] getMessageFragment(byte[] arr){
        byte[][] fragments = new byte[(int) Math.ceil(arr.length / (double)MAX_PAKET_LEN)][];

        for(int i = 0, j = 0; i < arr.length; i++){

            if(i == 0){
                if(arr.length >= MAX_PAKET_LEN){
                    fragments[j] = new byte[MAX_PAKET_LEN];
                }else{
                    fragments[j] = new byte[arr.length];
                }
            } else if((i % MAX_PAKET_LEN) == 0 ) {
                j++;
                if (arr.length - i > MAX_PAKET_LEN) {
                    fragments[j] = new byte[MAX_PAKET_LEN];
                } else {
                    fragments[j] = new byte[arr.length - i];
                }
            }

            fragments[j][i%MAX_PAKET_LEN] = arr[i];
        }

        return fragments;
    }
}
