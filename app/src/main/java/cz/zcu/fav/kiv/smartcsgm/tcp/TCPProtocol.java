package cz.zcu.fav.kiv.smartcsgm.tcp;

import android.app.UiAutomation;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import cz.zcu.fav.kiv.smartcsgm.data.Modules;
import cz.zcu.fav.kiv.smartcsgm.data.ModulesBufferData;
import cz.zcu.fav.kiv.smartcsgm.data.MovementData;

public class TCPProtocol {
    private static final byte[] MAGIC = new byte[]{0x47, (byte) 0xb0, 0x3f, (byte) 0xb3};
    private static final byte OPCODE_DATA = 1;
    private static final byte OPCODE_NONE = 0;

    private static final byte DATA_IDENTIFIER_NONE         = 0;
    private static final byte DATA_IDENTIFIER_TEMP_SKIN    = 1;
    private static final byte DATA_IDENTIFIER_TEMP_AMBIENT = 2;
    private static final byte DATA_IDENTIFIER_POS_LAT      = 3;
    private static final byte DATA_IDENTIFIER_POS_LON      = 4;

    private static final int VALUEN_LEN = 8;
    private static final int HEADER_LEN = 7;
    private static final int MAGIC_LEN = 4;

    private static final int ONE_DATA_LEN = 13;

    private static TCPProtocol INSTANCE;

    private TCPProtocol(){

    }

    public static TCPProtocol getInstance(){
        if(INSTANCE == null){
            INSTANCE = new TCPProtocol();
        }
        return INSTANCE;
    }

    public byte[] createMessage(ModulesBufferData modules){
        byte[] arrAmTemp = null;
        byte[] arrSkTemp = null;
        byte[] arrMov = null;
        byte[] arrLat = null;
        byte[] arrLon = null;

        byte[][] data = new byte[5][];



        if(modules.getModules().getTemp1Data() != null){
            ByteBuffer bbSkTemp = ByteBuffer.allocate(8);
            double celsSk = modules.getModules().getTemp1Data().getCels();
            bbSkTemp.order(ByteOrder.LITTLE_ENDIAN);
            bbSkTemp.putDouble(celsSk);
            arrSkTemp = bbSkTemp.array();
            data[0] = arrSkTemp;
        }

        if(modules.getModules().getTemp2Data() != null){
            ByteBuffer bbAmTemp = ByteBuffer.allocate(8);
            double celsAm = modules.getModules().getTemp2Data().getCels();
            bbAmTemp.order(ByteOrder.LITTLE_ENDIAN);
            bbAmTemp.putDouble(celsAm);
            arrAmTemp = bbAmTemp.array();
            data[1] = arrAmTemp;
        }

        if(modules.getModules().getGpsData() != null){
            ByteBuffer bbLat= ByteBuffer.allocate(8);
            double lat = modules.getModules().getGpsData().getLat();
            bbLat.order(ByteOrder.LITTLE_ENDIAN);
            bbLat.putDouble(lat);
            arrLat = bbLat.array();
            data[2] = arrLat;

            ByteBuffer bbLon = ByteBuffer.allocate(8);
            double lon = modules.getModules().getGpsData().getLon();
            bbLon.order(ByteOrder.LITTLE_ENDIAN);
            bbLon.putDouble(lon);
            arrLon = bbLon.array();
            data[3] = arrLon;
        }

        if(modules.getModules().getMoveData() != null){
            ByteBuffer bbMov = ByteBuffer.allocate(8);
            double move = calcAvargeMovement(modules.getModules().getMoveData());
            bbMov.order(ByteOrder.LITTLE_ENDIAN);
            bbMov.putDouble(move);
            arrMov = bbMov.array();
            data[4] = arrMov;
        }

        return buildMessageFromByteData(data, modules.getTimestamp());
    }


    private int getPayloadLen(byte[][] data){
        int mesLen = 0;

        for(int i = 0; i < data.length; i++){
            if(data[i] != null){
                mesLen += ONE_DATA_LEN;
            }
        }


        return mesLen;
    }

    private void buildMessageHeader(byte[] message, int mesLen){
        for(int i = 0; i < MAGIC_LEN; i++){
            message[i] = MAGIC[i];
        }

        ByteBuffer bb = ByteBuffer.allocate(4);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        bb.putInt(mesLen);
        byte[] mesLenArr = bb.array();


        message[MAGIC_LEN] = OPCODE_DATA;
        message[MAGIC_LEN + 1] = mesLenArr[0];
        message[MAGIC_LEN + 2] = mesLenArr[1];
        message[MAGIC_LEN + 3] = mesLenArr[2];
        message[MAGIC_LEN + 4] = mesLenArr[3];

    }

    private byte[] buildMessageFromByteData(byte[][] data, int timestamp){
        int mesLen = getPayloadLen(data);
        byte[] message = new byte[HEADER_LEN + mesLen];
        byte[] unitTimeStampArr;

        buildMessageHeader(message, mesLen);

        ByteBuffer bb = ByteBuffer.allocate(4);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        bb.putInt(timestamp);
        unitTimeStampArr = bb.array();

        for(int i = 0, j = 0; i < data.length; i++){

            if (data[i] != null){
                byte identifier = (byte) (i + 1); //identifier type
                message[HEADER_LEN + (j * ONE_DATA_LEN)] = identifier;
                message[HEADER_LEN + (j * ONE_DATA_LEN) + 1] = unitTimeStampArr[0];
                message[HEADER_LEN + (j * ONE_DATA_LEN) + 2] = unitTimeStampArr[1];
                message[HEADER_LEN + (j * ONE_DATA_LEN) + 3] = unitTimeStampArr[2];
                message[HEADER_LEN + (j * ONE_DATA_LEN) + 4] = unitTimeStampArr[3];

                for(int k = 0; k < data[i].length; k++){
                    message[HEADER_LEN + (j * ONE_DATA_LEN) + 5 + k] = data[i][k];
                }

                j++;
            }

        }

        return message;
    }

    private double calcAvargeMovement(MovementData movData){
        int sum = movData.getMovIn0() * 1 + movData.getMovIn1() * 2 + movData.getMovIn2() * 3 + movData.getMovIn3() * 4 + movData.getMovIn4() * 5;
        int valNum = movData.getMovIn0() + movData.getMovIn1() + movData.getMovIn2() + movData.getMovIn3() + movData.getMovIn4();
        double av = sum / (double)valNum;
        return av;
    }
}
