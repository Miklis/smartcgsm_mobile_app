package cz.zcu.fav.kiv.smartcsgm.data;

import java.util.Arrays;

public class MovementData {
    public static final int DATA_LEN = 27;

    private int movIn0;
    private int movIn1;
    private int movIn2;
    private int movIn3;
    private int movIn4;

    public static MovementData createData(byte[] arr){
        byte[] arr0 = Arrays.copyOfRange(arr, 3, 5);
        byte[] arr1 = Arrays.copyOfRange(arr, 8, 10);
        byte[] arr2 = Arrays.copyOfRange(arr, 13, 15);
        byte[] arr3 = Arrays.copyOfRange(arr, 18, 20);
        byte[] arr4 = Arrays.copyOfRange(arr, 23, 25);

        int mov0 = (int)java.nio.ByteBuffer.wrap(arr0).order(java.nio.ByteOrder.LITTLE_ENDIAN).getShort();
        int mov1 = (int)java.nio.ByteBuffer.wrap(arr1).order(java.nio.ByteOrder.LITTLE_ENDIAN).getShort();
        int mov2 = (int)java.nio.ByteBuffer.wrap(arr2).order(java.nio.ByteOrder.LITTLE_ENDIAN).getShort();
        int mov3 = (int)java.nio.ByteBuffer.wrap(arr3).order(java.nio.ByteOrder.LITTLE_ENDIAN).getShort();
        int mov4 = (int)java.nio.ByteBuffer.wrap(arr4).order(java.nio.ByteOrder.LITTLE_ENDIAN).getShort();

        return new MovementData(mov0, mov1, mov2, mov3, mov4);
    }

    public MovementData(int movIn0, int movIn1, int movIn2, int movIn3, int movIn4) {
        this.movIn0 = movIn0;
        this.movIn1 = movIn1;
        this.movIn2 = movIn2;
        this.movIn3 = movIn3;
        this.movIn4 = movIn4;
    }

    public void setMovIn0(int movIn0) {
        this.movIn0 = movIn0;
    }

    public void setMovIn1(int movIn1) {
        this.movIn1 = movIn1;
    }

    public void setMovIn2(int movIn2) {
        this.movIn2 = movIn2;
    }

    public void setMovIn3(int movIn3) {
        this.movIn3 = movIn3;
    }

    public void setMovIn4(int movIn4) {
        this.movIn4 = movIn4;
    }

    public int getMovIn0() {
        return movIn0;
    }

    public int getMovIn1() {
        return movIn1;
    }

    public int getMovIn2() {
        return movIn2;
    }

    public int getMovIn3() {
        return movIn3;
    }

    public int getMovIn4() {
        return movIn4;
    }
}
