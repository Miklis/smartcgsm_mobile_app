package cz.zcu.fav.kiv.smartcsgm.data;

public class Modules {
    private final AccelerometrData accData;
    private final GyroscopeData gyroData;
    private final TemperatureData temp1Data;
    private final TemperatureData temp2Data;
    private final GPSData gpsData;
    private final MovementData moveData;

    public Modules(AccelerometrData accData, GyroscopeData gyroData, TemperatureData temp1Data, TemperatureData temp2Data, GPSData gpsData, MovementData moveData) {
        this.accData = accData;
        this.gyroData = gyroData;
        this.temp1Data = temp1Data;
        this.temp2Data = temp2Data;
        this.gpsData = gpsData;
        this.moveData = moveData;
    }


    public AccelerometrData getAccData() {
        return accData;
    }

    public GyroscopeData getGyroData() {
        return gyroData;
    }

    public TemperatureData getTemp1Data() {
        return temp1Data;
    }

    public TemperatureData getTemp2Data() {
        return temp2Data;
    }

    public GPSData getGpsData() {
        return gpsData;
    }

    public MovementData getMoveData() {
        return moveData;
    }
}
