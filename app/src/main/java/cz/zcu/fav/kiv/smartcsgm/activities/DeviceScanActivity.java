package cz.zcu.fav.kiv.smartcsgm.activities;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.os.Handler;
import android.util.Log;
import android.view.Gravity;

import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashSet;

import cz.zcu.fav.kiv.smartcsgm.R;
import cz.zcu.fav.kiv.smartcsgm.dialogs.LoadingDialog;

public class DeviceScanActivity extends AppCompatActivity {
    private final String UNKNOWN = "Unknown";
    private BluetoothAdapter mBluetoothAdapter;
    private boolean mScanning;
    private Handler mHandler;
    private LoadingDialog loadingDialog;

    private LinearLayout deviceScrollView;
    private HashSet<BluetoothDevice> uniqBLEDev;

    private static final int REQUEST_ENABLE_BT = 1;
    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        mHandler = new Handler();

        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        loadingDialog = new LoadingDialog(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        checkCoarseLocationPermission();
        checkStoragePermission();
    }


    private boolean checkCoarseLocationPermission()
    {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            Log.i("checkCoarseLocation: ", "Has to request permissions to ACCESS_COARSE_LOCATION");
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            return false;
        }else
        {
            Log.i("checkCoarseLocation: ", "ACCESS_COARSE_LOCATION is already GRANTED");
            return true;
        }
    }

    private boolean checkStoragePermission(){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            Log.i("checkStoragePersmision:", "Has to request permissions to WRITE_EXTERNAL_STORAGE");
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            return false;
        }else
        {
            Log.i("checkStoragePersmision:", "WRITE_EXTERNAL_STORAGE is already GRANTED");
            return true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.findItem(R.id.action_settings).setVisible(false);
        if (!mScanning) {
            menu.findItem(R.id.menu_stop).setVisible(false);
            menu.findItem(R.id.menu_scan).setVisible(true);

        } else {
            menu.findItem(R.id.menu_stop).setVisible(true);
            menu.findItem(R.id.menu_scan).setVisible(false);
           // menu.findItem(R.id.menu_refresh).setActionView(R.layout.actionbar_indeterminate_progress);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_scan:
                deviceScrollView.removeAllViews();
                uniqBLEDev.clear();
                //mLeDeviceListAdapter.clear();
                scanLeDevice(true);
                break;
            case R.id.menu_stop:
                scanLeDevice(false);
                break;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();


        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        // Initializes list view adapter.
        this.uniqBLEDev = new HashSet<>();
        this.deviceScrollView = findViewById(R.id.scanned_device_container);

        if(deviceScrollView != null){
            deviceScrollView.removeAllViews();
        }

        scanLeDevice(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        scanLeDevice(false);
       // mLeDeviceListAdapter.clear();
    }

    /**
     * Start connection to BLE device and open DeviceControlActivity
     *
     * @param deviceName    name
     * @param deviceAddress address
     */
    private void onConnectClick(String deviceName, String deviceAddress) {
        final Intent intent = new Intent(this, DeviceControlActivity.class);
        intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_NAME, deviceName);
        intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_ADDRESS, deviceAddress);
        if (mScanning) {
            mBluetoothAdapter.getBluetoothLeScanner().stopScan(scanCallback);
            mScanning = false;
        }
        startActivity(intent);
    }


    private void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(() -> {
                mScanning = false;
                mBluetoothAdapter.getBluetoothLeScanner().stopScan(scanCallback);
                invalidateOptionsMenu();
                loadingDialog.dismissDialog();
            }, SCAN_PERIOD);

            loadingDialog.startLoadingDialog();
            mScanning = true;
            mBluetoothAdapter.getBluetoothLeScanner().startScan(scanCallback);
        } else {
            loadingDialog.dismissDialog();
            mScanning = false;
            mBluetoothAdapter.getBluetoothLeScanner().stopScan(scanCallback);
        }
        invalidateOptionsMenu();
    }


    /***
     * Creates layout for one scanned device
     *
     * @param device located device
     */
    private void addDeviceToScrollView(BluetoothDevice device){
        if(!uniqBLEDev.contains(device)){
            uniqBLEDev.add(device);

            LinearLayout deviceContainer = new LinearLayout(this);
            deviceContainer.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
            deviceContainer.setOrientation(LinearLayout.HORIZONTAL);


            LinearLayout deviceTextContainer = new LinearLayout(this);
            deviceTextContainer.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
            deviceTextContainer.setOrientation(LinearLayout.VERTICAL);

            TextView deviceName = new TextView(this);
            deviceName.setTypeface(null,Typeface.BOLD);

            if(device.getName() != null && device.getName().equals("") != true){
                deviceName.setText(device.getName());
            }else{
                deviceName.setText(UNKNOWN);
            }
            deviceName.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
            deviceTextContainer.addView(deviceName);

            TextView deviceMac = new TextView(this);
            deviceMac.setText(device.getAddress());
            deviceMac.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));

            deviceTextContainer.addView(deviceMac);

            Button join = new Button(this);
            LinearLayout.LayoutParams joinParams = new LinearLayout.LayoutParams(80, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
            joinParams.gravity = Gravity.RIGHT;

            join.setLayoutParams(joinParams);
            join.setText(R.string.connect);

            join.setOnClickListener(view1 -> {
                onConnectClick(device.getName(), device.getAddress());
            });

            Space space = new Space(this);
            space.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));


            deviceContainer.addView(deviceTextContainer);
            deviceContainer.addView(space);
            deviceContainer.addView(join);

            deviceScrollView.addView(deviceContainer);
        }

    }


    private ScanCallback scanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
           final BluetoothDevice device = result.getDevice();

            runOnUiThread(() -> {
                final String deviceName = device.getName();
                boolean condition;

                try
                {
                    condition=deviceName.equals("JDY-08");
                }
                catch(NullPointerException e)
                {
                    condition=false;
                }
                if (condition) {
                    autoConnect(device);
                } else {
                    addDeviceToScrollView(device);
                    //mLeDeviceListAdapter.addDevice(device);
                    //mLeDeviceListAdapter.notifyDataSetChanged();
                }
            });
            super.onScanResult(callbackType, result);
        }
    };


    private void autoConnect(BluetoothDevice device)
    {
      /*  mScanning = false;
        mBluetoothAdapter.getBluetoothLeScanner().stopScan(scanCallback);
        if (device == null) return;
        final Intent intent = new Intent(this, DeviceControlActivity.class);
        intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_NAME, device.getName());
        intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_ADDRESS, device.getAddress());
        if (mScanning) {
            mBluetoothAdapter.getBluetoothLeScanner().stopScan(scanCallback);
            mScanning = false;
        }
        startActivity(intent);
        finish();*/
    }
}