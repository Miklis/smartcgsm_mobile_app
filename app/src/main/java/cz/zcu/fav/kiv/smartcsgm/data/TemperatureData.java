package cz.zcu.fav.kiv.smartcsgm.data;

import java.util.Arrays;

public class TemperatureData {
    public static int DATA_LEN = 10;

    private final int cels;

    public static TemperatureData createData(byte[] arr){
        byte[] tempArr = Arrays.copyOfRange(arr, 3 , 5);

        int cels = (int)java.nio.ByteBuffer.wrap(tempArr).order(java.nio.ByteOrder.LITTLE_ENDIAN).getShort();

        return new TemperatureData(cels);

    }

    public TemperatureData(int cels) {
        this.cels = cels;
    }

    public int getCels() {
        return cels;
    }
}
