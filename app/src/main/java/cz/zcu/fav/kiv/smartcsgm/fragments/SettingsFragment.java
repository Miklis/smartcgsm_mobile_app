package cz.zcu.fav.kiv.smartcsgm.fragments;

import android.graphics.Typeface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;

import cz.zcu.fav.kiv.smartcsgm.InputFilterMinMax;
import cz.zcu.fav.kiv.smartcsgm.R;
import cz.zcu.fav.kiv.smartcsgm.activities.DeviceControlActivity;
import cz.zcu.fav.kiv.smartcsgm.data.ModuleSettingUI;
import cz.zcu.fav.kiv.smartcsgm.data.ModuleSettings;
import cz.zcu.fav.kiv.smartcsgm.bluetooth.BLEProtocol;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class SettingsFragment extends Fragment {
    private final int MIN_READ_INTERVAL_VALUE = 50;
    private final int MAX_READ_INTERVAL_VALUE = 1000;
    private final String READ_INTERVAL_TEXT = "read int:";

    private ArrayList<ModuleSettingUI> modules;
    private LinearLayout deviceContainerScroll;
    private Button setButton;
    private Button getInfoButton;
    private DeviceControlActivity deviceControlActivity;
    private EditText mainLoopSettings;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        BLEProtocol.getInstance().setSettingsFragment(this);
        deviceControlActivity = (DeviceControlActivity) getContext();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view =  inflater.inflate(R.layout.fragment_settings, container, false);
        modules = null;
        deviceContainerScroll = view.findViewById(R.id.device_set_container);
        setButton = view.findViewById(R.id.button_set_modules);
        getInfoButton = view.findViewById(R.id.button_get_modules_info);
        mainLoopSettings = view.findViewById(R.id.editTextNumber_main_loop);
        addSetButtonListener(getInfoButton);
        addSendSettingsButtonListener(setButton);
        setMainLoopFilter();
        setMainLoopButtonSetListenner(view);
        return view;
    }


    private void setMainLoopFilter(){
        mainLoopSettings.setFilters(new InputFilterMinMax[]{new InputFilterMinMax(0, MAX_READ_INTERVAL_VALUE)});
    }


    private void setMainLoopButtonSetListenner(View view){
        Button button = view.findViewById(R.id.button_main_loop);
        button.setOnClickListener(view1 -> {
            if(deviceControlActivity != null && mainLoopSettings != null){
                int mainLoopInterval = 0;
                try{
                    mainLoopInterval = Integer.parseInt(mainLoopSettings.getText().toString());
                }catch (NumberFormatException e){
                    Toast.makeText(deviceControlActivity, "Loop interval: Not number", Toast.LENGTH_LONG);
                }

                if(mainLoopInterval > MAX_READ_INTERVAL_VALUE || mainLoopInterval < MIN_READ_INTERVAL_VALUE){
                    Toast.makeText(deviceControlActivity, "Loop interval: invalid value", Toast.LENGTH_LONG);
                    return;
                }

                deviceControlActivity.writeCharacteristic(BLEProtocol.getInstance().buildMainLoopSettingsMessage(mainLoopInterval));
            }
        });
    }

    private void addSetButtonListener(Button button){
        button.setOnClickListener(view -> deviceControlActivity.writeCharacteristic(BLEProtocol.getInstance().buildInfoReqMessage()));
    }

    private  void addSendSettingsButtonListener(Button button){
        button.setOnClickListener(view -> {
            if(this.modules != null){
                if(modules != null){
                    byte[][] messageFragment = BLEProtocol.getInstance().buildModulesSettingMessage(modules);

                    for(int i = 0; i < messageFragment.length; i++){
                        deviceControlActivity.writeCharacteristic(messageFragment[i]);
                    }
                }
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.menu_data).setVisible(true);
        menu.findItem(R.id.menu_settings_info).setVisible(false);
    }

    public void addDeviceInfo(ArrayList<ModuleSettings> modules){
       // modules = new HashSet<>();
        deviceContainerScroll.removeAllViews();
        this.modules = new ArrayList<>();
        for(ModuleSettings moduleSettings : modules){
            LinearLayout deviceContainer = new LinearLayout(getContext());
            deviceContainer.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
            deviceContainer.setOrientation(LinearLayout.VERTICAL);

            LinearLayout infoLayout = new LinearLayout(getContext());
            infoLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
            infoLayout.setOrientation(LinearLayout.HORIZONTAL);

            LinearLayout settingsLayout = new LinearLayout(getContext());
            settingsLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
            settingsLayout.setOrientation(LinearLayout.HORIZONTAL);

            //Info
            TextView name = new TextView(getContext());
            name.setTypeface(null, Typeface.BOLD);
            name.setText(moduleSettings.getModuleName());
            name.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
            infoLayout.addView(name);

            TextView id = new TextView(getContext());
            id.setText(String.format("%d", moduleSettings.getModuleID()));
            id.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
            infoLayout.addView(id);

            TextView type = new TextView(getContext());
            type.setText(moduleSettings.getModuleType().name());
            type.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
            infoLayout.addView(type);


            //Settings

            TextView readInt = new TextView(getContext());
            readInt.setText(READ_INTERVAL_TEXT);
            readInt.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
            settingsLayout.addView(readInt);

            TextInputEditText readIntevalVal = new TextInputEditText(getContext());
            readIntevalVal.setFilters(new InputFilter[]{ new InputFilterMinMax(moduleSettings.getMinReadInt(), moduleSettings.getMaxReadInt())});
            readIntevalVal.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
            readIntevalVal.setText(moduleSettings.getReadInterval() + "");
            settingsLayout.addView(readIntevalVal);

            CheckBox sleepBox = new CheckBox(getContext());
            sleepBox.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
            sleepBox.setChecked(moduleSettings.isSleep());
            settingsLayout.addView(sleepBox);

            deviceContainer.addView(infoLayout);
            deviceContainer.addView(settingsLayout);

            deviceContainerScroll.addView(deviceContainer);

            this.modules.add(new ModuleSettingUI(id, readIntevalVal, sleepBox));
        }

        //modules.add(moduleSettings);
    }
}