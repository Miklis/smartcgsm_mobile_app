package cz.zcu.fav.kiv.smartcsgm.bluetooth;

public enum ModuleType {
    ACCELOMETR,
    GYROSCOPE,
    TEMP,
    PULSE,
    GPS,
    MOVEMENT;

    public static ModuleType getModuleType(int iType){
        switch (iType){
            case 1:
                return ACCELOMETR;
            case 2:
                return GYROSCOPE;
            case 3:
                return TEMP;
            case 4:
                return PULSE;
            case 5:
                return GPS;
            case 6:
                return MOVEMENT;
            default:
                return null;
        }
    }


}
