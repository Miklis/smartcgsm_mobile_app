package cz.zcu.fav.kiv.smartcsgm.data;

import java.lang.reflect.Array;
import java.util.Arrays;

public class AccelerometrData {
     public static int DATA_LEN = 17;

    private final int x;
    private final int y;
    private final int z;

    public static AccelerometrData createData(byte[] arr){
        byte[] arrX = Arrays.copyOfRange(arr, 3, 5);
        byte[] arrY = Arrays.copyOfRange(arr, 8, 10);
        byte[] arrZ = Arrays.copyOfRange(arr, 13, 15);

        int x = (int)java.nio.ByteBuffer.wrap(arrX).order(java.nio.ByteOrder.LITTLE_ENDIAN).getShort();
        int y = (int)java.nio.ByteBuffer.wrap(arrY).order(java.nio.ByteOrder.LITTLE_ENDIAN).getShort();
        int z = (int)java.nio.ByteBuffer.wrap(arrZ).order(java.nio.ByteOrder.LITTLE_ENDIAN).getShort();

        return new AccelerometrData(x, y, z);
    }

    public AccelerometrData(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public int getX() {
        return x;
    }
}
