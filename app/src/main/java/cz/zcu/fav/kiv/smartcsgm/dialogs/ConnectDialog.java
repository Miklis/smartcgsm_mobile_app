package cz.zcu.fav.kiv.smartcsgm.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.google.android.material.textfield.TextInputEditText;

import cz.zcu.fav.kiv.smartcsgm.R;
import cz.zcu.fav.kiv.smartcsgm.activities.DeviceControlActivity;
import cz.zcu.fav.kiv.smartcsgm.tcp.TCPClient;

public class ConnectDialog extends AppCompatDialogFragment  {

    DeviceControlActivity deviceControlActivity;

    public ConnectDialog(DeviceControlActivity deviceControlActivity) {
        this.deviceControlActivity = deviceControlActivity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_connect, null);

        TextInputEditText ip = view.findViewById(R.id.textInputEditText_IP);
        TextInputEditText port = view.findViewById(R.id.textInputEditText_Port);

        builder.setView(view)
                .setTitle("Connect")
                .setNegativeButton("cancel", (dialogInterface, i) -> {

                })
                .setPositiveButton("connect", (dialogInterface, i) -> {
                    String ipStr = ip.getEditableText().toString();
                    String portStr = port.getEditableText().toString();

                    if (checkIp(ipStr) == true && checkPort(portStr)){

                        TCPClient.initialize(ipStr, Integer.parseInt(portStr), deviceControlActivity);

                    }else{
                        Toast.makeText(getContext(), "Invalid parametrs", Toast.LENGTH_LONG);
                    }
                });

        return builder.create();
    }

    /**
     * Check if ip is valid
     */
    public boolean checkIp(String ip){
        String PATTERN = "^((0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)\\.){3}(0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)$";

        return ip.matches(PATTERN);
    }


    /**
     * Check if port is from range 0-65535
     */
    public boolean checkPort(String port){
        String PATTERN = "^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$";

        return port.matches(PATTERN);
    }


}
